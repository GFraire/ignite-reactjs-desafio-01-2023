import { Header } from "./components/Header";
import { ToDoContent } from "./components/ToDoContent";
import styles from "./App.module.css";

function App() {
  return (
    <div className={styles.wrapper}>
      <Header />

      <ToDoContent />
    </div>
  );
}

export default App;
