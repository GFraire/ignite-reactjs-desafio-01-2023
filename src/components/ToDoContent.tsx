import { ClipboardText, PlusCircle, Trash, Check } from "@phosphor-icons/react";
import styles from "./ToDoContent.module.css";
import { ChangeEvent, useState } from "react";
import { v4 as uuidv4 } from "uuid";

interface ListItem {
  id: string;
  title: string;
  isFinished: boolean;
}

export function ToDoContent() {
  const [listItems, setListItems] = useState([] as ListItem[]);

  const [itemTitle, setItemTitle] = useState("");

  const isCreateButtonDisabled = itemTitle.length == 0;
  const createdTasksCount = listItems.length;
  const finishedTasksCount = listItems.filter(
    (listItem) => listItem.isFinished == true
  ).length;

  function handleCreateListItem() {
    const newListItem: ListItem = {
      id: uuidv4(),
      isFinished: false,
      title: itemTitle,
    };

    setListItems([...listItems, newListItem]);

    setItemTitle("");
  }

  function handleChangeItemTitle(event: ChangeEvent<HTMLInputElement>) {
    setItemTitle(event.target.value);
  }

  function handleDeleteListItem(id: string) {
    const newListItems = listItems.filter((listItem) => listItem.id != id);

    setListItems(newListItems);
  }

  function handleFinishListItem(
    event: ChangeEvent<HTMLInputElement>,
    id: string
  ) {
    const newListItems = listItems.map((listItem) => {
      if (listItem.id == id) listItem.isFinished = event.target.checked;

      return listItem;
    });

    setListItems(newListItems);
  }

  return (
    <div className={styles.container}>
      <div className={styles.toDoCreate}>
        <input
          onChange={handleChangeItemTitle}
          value={itemTitle}
          type="text"
          placeholder="Adicione uma tarefa"
        />

        <button
          onClick={handleCreateListItem}
          disabled={isCreateButtonDisabled}
        >
          Criar <PlusCircle size={16} />
        </button>
      </div>

      <div className={styles.toDoList}>
        <div className={styles.toDoHeader}>
          <span>
            Tarefas criadas <strong>{createdTasksCount}</strong>
          </span>

          <span>
            Concluídas{" "}
            <strong>
              {finishedTasksCount == 0
                ? finishedTasksCount
                : `${finishedTasksCount} de ${listItems.length}`}
            </strong>
          </span>
        </div>

        {createdTasksCount == 0 ? (
          <div className={styles.todoListEmpty}>
            <ClipboardText
              className={styles.icon}
              size={56}
              color="var(--gray-400)"
            />

            <strong>Você ainda não tem tarefas cadastradas</strong>

            <span>Crie tarefas e organize seus itens a fazer</span>
          </div>
        ) : (
          <div className={styles.toDoListItems}>
            {listItems.map((listItem) => (
              <div key={listItem.id}>
                <div className={styles.toDoListItemInfo}>
                  <label className={styles.checkboxContainer}>
                    <input
                      onChange={(event) =>
                        handleFinishListItem(event, listItem.id)
                      }
                      type="checkbox"
                    />
                    <span className={styles.checkmark}>
                      <span>
                        <Check
                          weight="bold"
                          size={16}
                          color="var(--gray-200)"
                        />
                      </span>
                    </span>
                  </label>

                  <span
                    className={listItem.isFinished ? styles.strokedText : ""}
                  >
                    {listItem.title}
                  </span>
                </div>

                <Trash
                  onClick={() => handleDeleteListItem(listItem.id)}
                  size={16}
                  cursor="pointer"
                />
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
}
